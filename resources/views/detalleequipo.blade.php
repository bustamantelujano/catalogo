@extends('layouts.app')


@section('head')
  <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->

	<title>Catálogo de laptops</title>
  {{-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"> --}}

@endsection


@section('content')

      <!-- Page Content -->
      <div class="container" >

          <!-- Portfolio Item Heading -->
          <div class="row">
              <div class="col-lg-12">
                  <h1 class="page-header">{{$equipo->titulo}}
                      <small>${{$equipo->precio}}</small>
                  </h1>
              </div>
          </div>
          <!-- /.row -->

          <!-- Portfolio Item Row -->
          <div class="row">

              <div class="col-md-8">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner">
                        <input type="hidden" name="" value="{{$flag = true}}">
                        @foreach ($images as $is)
                          @if ($flag)
                            <div class="item active">

                          @else
                            <div class="item ">

                          @endif
                            <a href="/images/{{$is->filename}}" id="pop">
                                <img id="imageresource" src="/images/{{$is->filename}}" >
                            </a>
                          </div>
                          <input type="hidden" name="" value="{{$flag = false}}">

                        @endforeach

                      </div>

                      <!-- Controls -->
                      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <div class="">
                          <span class="glyphicon glyphicon-chevron-left" style=""></span>
                        </div>
                      </a>
                      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                      </a>
                    </div>
              </div>

              <div class="col-md-4">
                  <h3>Descripción</h3>
                  <p>{{str_replace(' ', '&nbsp;',$equipo->desc)}}</p>
                  {{-- <h3>Project Details</h3>
                  <ul>
                      <li>Lorem Ipsum</li>
                      <li>Dolor Sit Amet</li>
                      <li>Consectetur</li>
                      <li>Adipiscing Elit</li>
                  </ul> --}}
              </div>

          </div>
          <!-- /.row -->

          <!-- Related Projects Row -->

          @if (count($promo))
            <div class="row">

                <div class="col-lg-12">
                    <h3 class="page-header"><img src="/images/dis.png"  style="height:30px;margin-top:-5px" alt=""> En promoción </h3>
                </div>

                @foreach ($promo as $pr)
                  <div class="col-sm-3 col-xs-6" style="margin-bottom:30px; " >
                      <a href="/equipos/{{$pr->id}}">
                          <img class="img-responsive portfolio-item" src="/images/{{$pr->img_principal}}" alt="">
                      </a>
                      <h5>{{$pr->titulo}}  ${{$pr->precio}}</h5>
                  </div>
                @endforeach


            </div>
          @endif

          <!-- /.row -->

          <hr>

          <!-- Footer -->
          <footer>
              <div class="row">
                  <div class="col-lg-12">
                      {{-- <p>Copyright &copy; Mad Macs 2017</p> --}}
                  </div>
              </div>
              <!-- /.row -->
          </footer>

      </div>
      <!-- /.container -->

      <!-- jQuery -->
      <script src="js/jquery.js"></script>

      <!-- Bootstrap Core JavaScript -->
      <script src="js/bootstrap.min.js"></script>










     </div><!-- end row-->



<style media="screen">
/* Makes images fully responsive */

.img-responsive,
.thumbnail > img,
.thumbnail a > img,
.carousel-inner > .item > img,
.carousel-inner > .item > a > img {
  display: block;
  width: 100%;
  height: auto;
}

/* ------------------- Carousel Styling ------------------- */

.carousel-inner {
  border-radius: 3px;
}

.carousel-caption {
  background-color: rgba(0,0,0,.5);
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  padding: 0 0 10px 25px;
  color: #fff;
  text-align: left;
}

.carousel-indicators {
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  width: 100%;
  z-index: 15;
  margin: 0;
  padding: 0 25px 25px 0;
  text-align: right;
}

.carousel-control.left,
.carousel-control.right {
  background-image: none;

}


/* ------------------- Section Styling - Not needed for carousel styling ------------------- */

.section-white {
   padding: 10px 0;
}

.section-white {
  background-color: #fff;
  color: #555;
}

@media screen and (min-width: 768px) {

  .section-white {
     padding: 1.5em 0;
  }

}

@media screen and (min-width: 992px) {

  .container {
    max-width: 930px;
  }

}
</style>
<script type="text/javascript">
$("#pop").on("click", function() {
   $('#imagepreview').attr('src', $('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});
</script>

@endsection
