<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Catálogo de laptops</title>
    <meta name="description" content="Computadoras seminuevas al mejor precio">
    <meta name="author" content="Manuel Bustamante Lujano">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script> --}}
        {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script> --}}
        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous"> --}}
        <script src="https://lapsproject.herokuapp.com/js/jquery.unveil.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery_lazyload/1.9.7/jquery.lazyload.js"></script>

    <link href="https://lapsproject.herokuapp.com/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{url('/')}}/css/templatemo-style.css" rel="stylesheet"> --}}
    <script type="text/javascript" src="https://lapsproject.herokuapp.com/js/jquery-1.11.2.min.js"></script>      <!-- jQuery -->
    <script type="text/javascript" src="https://lapsproject.herokuapp.com/js/jquery-migrate-1.2.1.min.js"></script>      <!-- jQuery -->
    <script type="text/javascript" src="https://lapsproject.herokuapp.com/js/jquery.js"></script>      <!-- jQuery -->
    {{-- <script type="text/javascript" src="{{url('/')}}/js/templatemo-script.js"></script>      <!-- Templatemo Script --> --}}
    <script type="text/javascript" src="https://lapsproject.herokuapp.com/js/bootstrap.js"></script>      <!-- Templatemo Script -->
    {{-- <script type="text/javascript" src="{{url('/')}}/js/dropzone.js"></script>      <!-- Templatemo Script --> --}}
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous"> --}}

    {{-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> --}}
    {{-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script> --}}
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Catálogo de laptops <i class="fa fa-laptop"></i></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav navbar-right">
        @if (!Auth::guest())
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{Auth::user()->name}} <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">

              <li><a href="/admin"><i class="fa fa-laptop"></i> Mis equipos </a></li>

              <li><a href="/admin/users"><i class="fa fa-user"></i> Administradores </a></li>

              <li class="divider"></li>
              <li>
                <a href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i>  Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
        @endif

      </ul>
    </div>
  </div>
</nav>
    <!-- Left column -->
    <div class="templatemo-flex-row" style="margin-top:80px;">

      <!-- Main content -->
      <div class="templatemo-content col-1 light-gray-bg" style="min-height:0px">
        <!--
        <div class="templatemo-top-nav-container"></div>
        <br>

      -->

        @yield('content')


<style media="screen">
  img.hidden { display: none; }
</style>


        <script type="text/javascript">
        (function($){
  $.fn.reveal = function(){
      var args = Array.prototype.slice.call(arguments);
      return this.each(function(){
          var img = $(this),
              src = img.data("src");
          src && img.attr("src", src).load(function(){
              img[args[0]||"show"].apply(img, args.splice(1));
          });
      });
  }
})(jQuery);


$(function(){
  $("img.hidden").reveal("fadeIn", 1000);
});
        </script>
      </div>
    </div>



  </body>
</html>
