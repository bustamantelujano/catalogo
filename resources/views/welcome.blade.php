@extends('layouts.app')


@section('head')
  <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->

	<title>Catálogo de laptops</title>

@endsection



@section('content')

  {{-- <img height="600" width="900"  class="scale" src="https://s.cdpn.io/23379/photo-camping_480x320bw.jpg" data-src="https://s.cdpn.io/23379/photo-camping_2880x1920.jpg" alt="camping" /> --}}

  <ul class="container-fluid" style="margin-top:50px; ">



    <div class="">




  @foreach ($equipos as $e)


    <div class="col-sm-6 col-md-4 col-lg-3 " style="margin-bottom:10px; min-height:400px">
        <div class="card card-inverse card-info">
            <img class="card-img-top" src="/images/{{$e->img_principal}}">
            {{-- <img class="card-img-top" data-src="https://unsplash.it/900/500?image=142"  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAACCAYAAACddGYaAAAMFWlDQ1BJQ0MgUHJvZmlsZQAASImVVwdUk8kWnr+kEBJaIAJSQu9Ir9J7EZAOFghJgFAiJAQVO7Ko4FpQsWBFV0VUXAsgi4qIYmERsNcHKior62LBhsqbFND1tfPuOfPPlzv33vnu5M6cGQAUbVh5eTmoEgC5/AJBdJAvMzEpmUnqA0QwEVCBMdBnsYV5PlFR4QDKWP93eXcTIOL+mpU41r+O/1dR5nCFbACQKIjTOEJ2LsTHAcDV2XmCAgAIHVBvMLsgT4yHIFYVQIIAEHExzpBidTFOk2JLiU1stB/E3gCQqSyWIAMABTFvZiE7A8ZREHO04XN4fIi3QuzJzmRxIL4PsWVu7iyIFckQm6Z9FyfjbzHTxmOyWBnjWJqLRMj+PGFeDmvu/7kc/1tyc0Rjc+jDRs0UBEeLc4brtj97VpgYUyFu5qdFREKsAvFFHkdiL8Z3M0XBcTL7QbbQD64ZYACAAg7LPwxiLYgZouw4Hxm2YwkkvtAejeAVhMTKcJpgVrQsPlrIz4kIl8VZnskNGcPbucKAmDGbdF5gCMSw0tDjRZmxCVKeaFshLz4CYgWIu4TZMWEy34dFmX4RYzYCUbSYsyHEb9MFgdFSG0w9VziWF2bNZknmgrWAeRdkxgZLfbFErjAxfIwDh+sfIOWAcbj8OBk3DFaXb7TMtzQvJ0pmj23n5gRFS9cZOyIsjBnz7SmABSZdB+xRFis0SjbXu7yCqFgpNxwF4cAP+AMmEMGWBmaBLMDrHGwYhL+kI4GABQQgA3CBlUwz5pEgGeHDbwwoAn9CxAXCcT9fySgXFEL9l3Gt9GsF0iWjhRKPbPAU4lxcE/fE3fFw+PWGzQ53wV3H/JiKY7MSA4j+xGBiINFsnAcbss6BTQB4/0YXBnsuzE7MhT+Ww7d4hKeEbsIjwg1CL+EOiAdPJFFkVim8YsEPzJlgCuiF0QJl2aV9nx1uDFk74r64B+QPueMMXBNY4Q4wEx/cC+bmCLXfMxSNc/u2lj/OJ2b9fT4yvYK5gqOMRdr4P+M3bvVjFL/v1ogD+7AfLbHl2DGsHTuLXcKasQbAxM5gjVgHdkqMxyvhiaQSxmaLlnDLhnF4YzY2tTYDNp9/mJslm1+8XsIC7pwC8Wbwm5U3V8DLyCxg+sDTmMsM4bOtLZl2NrbOAIjPdunR8YYhObMRxuVvuvwWAFzLoDLjm45lAMDJpwDQ333TGbyG5b4GgFNdbJGgUKoTH8eAAChAEe4KDaADDIApzMcOOAF34A0CQCiIBLEgCcyEK54JciHn2WA+WAJKQTlYAzaALWAH2A32g0PgKGgAzeAsuACugC5wA9yDddEPXoAh8A6MIAhCQmgIHdFAdBEjxAKxQ1wQTyQACUeikSQkFclA+IgImY8sRcqRCmQLsgupQX5FTiJnkUtIN3IH6UMGkNfIJxRDqagqqo0ao5NQF9QHDUNj0RloBpqPFqEl6Cp0E1qNHkTr0bPoFfQG2ou+QIcxgMljDEwPs8JcMD8sEkvG0jEBthArwyqxauww1gT/52tYLzaIfcSJOB1n4lawNoPxOJyN5+ML8ZX4Fnw/Xo+34dfwPnwI/0qgEbQIFgQ3QgghkZBBmE0oJVQS9hJOEM7DfdNPeEckEhlEE6Iz3JdJxCziPOJK4jZiHbGF2E18TBwmkUgaJAuSBymSxCIVkEpJm0kHSWdIPaR+0geyPFmXbEcOJCeT+eRiciX5APk0uYf8jDwipyRnJOcmFynHkZsrt1puj1yT3FW5frkRijLFhOJBiaVkUZZQNlEOU85T7lPeyMvL68u7yk+V58kvlt8kf0T+onyf/EeqCtWc6kedThVRV1H3UVuod6hvaDSaMc2blkwroK2i1dDO0R7SPijQFawVQhQ4CosUqhTqFXoUXirKKRop+ijOVCxSrFQ8pnhVcVBJTslYyU+JpbRQqUrppNItpWFlurKtcqRyrvJK5QPKl5Sfq5BUjFUCVDgqJSq7Vc6pPKZjdAO6H51NX0rfQz9P71clqpqohqhmqZarHlLtVB1SU1FzUItXm6NWpXZKrZeBMYwZIYwcxmrGUcZNxqcJ2hN8JnAnrJhweELPhPfqE9W91bnqZep16jfUP2kwNQI0sjXWajRoPNDENc01p2rO1tyueV5zcKLqRPeJ7IllE49OvKuFaplrRWvN09qt1aE1rK2jHaSdp71Z+5z2oA5Dx1snS2e9zmmdAV26rqcuT3e97hndP5hqTB9mDnMTs405pKelF6wn0tul16k3om+iH6dfrF+n/8CAYuBikG6w3qDVYMhQ13CK4XzDWsO7RnJGLkaZRhuN2o3eG5sYJxgvM24wfm6ibhJiUmRSa3LflGbqZZpvWm163Yxo5mKWbbbNrMscNXc0zzSvMr9qgVo4WfAstll0WxIsXS35ltWWt6yoVj5WhVa1Vn3WDOtw62LrBuuXkwwnJU9aO6l90lcbR5scmz0292xVbENti22bbF/bmdux7arsrtvT7APtF9k32r9ysHDgOmx3uO1Id5ziuMyx1fGLk7OTwOmw04CzoXOq81bnWy6qLlEuK10uuhJcfV0XuTa7fnRzcitwO+r2l7uVe7b7Affnk00mcyfvmfzYQ9+D5bHLo9eT6ZnqudOz10vPi+VV7fXI28Cb473X+5mPmU+Wz0Gfl742vgLfE77v/dz8Fvi1+GP+Qf5l/p0BKgFxAVsCHgbqB2YE1gYOBTkGzQtqCSYEhwWvDb4Voh3CDqkJGQp1Dl0Q2hZGDYsJ2xL2KNw8XBDeNAWdEjpl3ZT7EUYR/IiGSBAZErku8kGUSVR+1G9TiVOjplZNfRptGz0/uj2GHpMScyDmXaxv7OrYe3GmcaK41njF+OnxNfHvE/wTKhJ6EyclLki8kqSZxEtqTCYlxyfvTR6eFjBtw7T+6Y7TS6ffnGEyY86MSzM1Z+bMPJWimMJKOZZKSE1IPZD6mRXJqmYNp4WkbU0bYvuxN7JfcLw56zkDXA9uBfdZukd6RfrzDI+MdRkDmV6ZlZmDPD/eFt6rrOCsHVnvsyOz92WP5iTk1OWSc1NzT/JV+Nn8tlk6s+bM6s6zyCvN6813y9+QPyQIE+wVIsIZwsYCVXjN6RCZin4S9RV6FlYVfpgdP/vYHOU5/Dkdc83nrpj7rCiw6Jd5+Dz2vNb5evOXzO9b4LNg10JkYdrC1kUGi0oW9S8OWrx/CWVJ9pLfi22KK4rfLk1Y2lSiXbK45PFPQT/VliqUCkpvLXNftmM5vpy3vHOF/YrNK76Wccoul9uUV5Z/Xsleefln2583/Ty6Kn1V52qn1dvXENfw19xc67V2f4VyRVHF43VT1tWvZ64vW/92Q8qGS5UOlTs2UjaKNvZuCt/UuNlw85rNn7dkbrlR5VtVt1Vr64qt77dxtvVs995+eIf2jvIdn3bydt7eFbSrvtq4unI3cXfh7qd74ve0/+LyS81ezb3le7/s4+/r3R+9v63GuabmgNaB1bVorah24OD0g12H/A81HrY6vKuOUVd+BBwRHfnj19Rfbx4NO9p6zOXY4eNGx7eeoJ8oq0fq59YPNWQ29DYmNXafDD3Z2uTedOI369/2Nes1V51SO7X6NOV0yenRM0VnhlvyWgbPZpx93JrSeu9c4rnrbVPbOs+Hnb94IfDCuXaf9jMXPS42X3K7dPKyy+WGK05X6jscO0787vj7iU6nzvqrzlcbu1y7mrond5/u8eo5e83/2oXrIdev3Ii40X0z7ubtW9Nv9d7m3H5+J+fOq7uFd0fuLb5PuF/2QOlB5UOth9X/MPtHXa9T76k+/76ORzGP7j1mP37xRPjkc3/JU9rTyme6z2qe2z1vHggc6Ppj2h/9L/JejAyW/qn859aXpi+P/+X9V8dQ4lD/K8Gr0dcr32i82ffW4W3rcNTww3e570bel33Q+LD/o8vH9k8Jn56NzP5M+rzpi9mXpq9hX++P5o6O5rEELMlVAIMNTU8H4PU+AGhJ8O7QBQBFQfr2kggifS9KEPhPWPo+k4gTAPu8AYhbDEA4vKNsh80IYirsxVfvWG+A2tuPN5kI0+3tpLGo8AVD+DA6+kYbAFITAF8Eo6Mj20ZHv+yBZO8A0JIvffOJhQjv9zvpYnRlTspB8IP8E+kgbMeVANkpAAAACXBIWXMAABYlAAAWJQFJUiTwAAACAWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczpleGlmPSJodHRwOi8vbnMuYWRvYmUuY29tL2V4aWYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8ZXhpZjpQaXhlbFlEaW1lbnNpb24+NDwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4xNjwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgocjOjDAAAAFElEQVQIHWOMPXjtPwMUMMEYIBoARJ8C9w4OyPYAAAAASUVORK5CYII=" alt=""> --}}
            <div class="card-block">
                @if ($e->promo == 1)
                  <figure class="profile">
                      <img src="/images/dis.png" class="profile-avatar" alt="">
                  </figure>
                @endif
                <a href="/equipos/{{$e->id}}"><h4 class="  card-title mt-3" style="display: block; width: 100%">
                  @if (strlen($e->titulo) > 35)
                    {{substr($e->titulo,0,35)}}...
                  @else
                    {{$e->titulo}}
                  @endif
                </h4>
                </a>
                {{-- <div class="meta card-text">
                    <a>Precio</a>
                </div>
                <div class="card-text">
                    text
                </div> --}}
            </div>
            <div class="card-footer">
              <div class="pull-left" style="margin-bottom:10px">
                <a href="/equipos/{{$e->id}}">
                  <h4 >${{$e->precio}}</h4>

                </a>
              </div>
              <div class="pull-right" style="margin-bottom:10px">
                <a href="/equipos/{{$e->id}}">
                  <button class="btn btn-info float-right btn">Detalles</button>

                </a>
              </div>

            </div>
        </div>
    </div>


    {{-- <div class="card col-md-3">
      <img class="card-img-top" src="..." alt="Card image cap">
      <div class="card-block">
        <h4 class="card-title">Card title</h4>
        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
      </div>
    </div> --}}

     {{-- <li class="card" id="" style="margin:20px;  ">
        <a href="/equipos/{{$e->id}}" class=""></a>
        <div class="item">

           <img src="images/s1.jpg" style="@if ($e->promo != null) border:-2px solid red; @endif" alt="Item Preview">
            </div>

        </a>

        <a href="/equipos/{{$e->id}}" class="cd-trigger" @if ($e->promo != null) style="background-color:rgba(66, 180, 125, 0.80) ; " @endif >
          @if ($e->promo != null) <strong>*PROMO*</strong>  @endif

          {{$e->titulo}} (${{$e->precio}})

        </a>


  		</li> <!-- cd-item --> --}}



  @endforeach


</div>


</ul> <!-- cd-items -->


<style media="screen">


.item {
position: relative;
top: 0;
left: 0;
}
.image1 {
position: relative;
top: 0;
left: 0;
}
.image2 {
position: absolute;
top: 30px;
left: 70px;
}

html {
    font-family: Lato, 'Helvetica Neue', Arial, Helvetica, sans-serif;
    font-size: 14px;
}

h5 {
    font-size: 1.28571429em;
    font-weight: 700;
    line-height: 1.2857em;
    margin: 0;
}

.card {
    font-size: 1em;
    overflow: hidden;
    padding: 0;
    border: none;
    border-radius: .28571429rem;
    box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
}

.card-block {
    font-size: 1em;
    position: relative;
    margin: 0;
    padding: 1em;
    border: none;
    border-top: 1px solid rgba(34, 36, 38, .1);
    box-shadow: none;
}

.card-img-top {
    display: block;
    width: 100%;
    height: auto;
}

.card-title {
    font-size: 1.28571429em;
    font-weight: 700;
    line-height: 1.2857em;
}

.card-text {
    clear: both;
    margin-top: .5em;
    color: rgba(0, 0, 0, .68);
}

.card-footer {
    font-size: 1em;
    position: static;
    top: 0;
    left: 0;
    max-width: 100%;
    padding: .75em 1em;
    color: rgba(0, 0, 0, .4);
    border-top: 1px solid rgba(0, 0, 0, .05) !important;
    background: #fff;
}

.card-inverse .btn {
    border: 1px solid rgba(0, 0, 0, .05);
}

.profile {
    position: absolute;
    top: -22px;
    display: inline-block;
    overflow: hidden;
    box-sizing: border-box;
    width: 45px;
    height: 45px;
    margin: 0;
    border: 1px solid #fff;
    border-radius: 50%;
}

.profile-avatar {
    display: block;
    width: 100%;
    height: auto;
    border-radius: 50%;
}

.profile-inline {
    position: relative;
    top: 0;
    display: inline-block;
}

.profile-inline ~ .card-title {
    display: inline-block;
    margin-left: 4px;
    vertical-align: top;
}

.text-bold {
    font-weight: 700;
}

.meta {
    font-size: 1em;
    color: rgba(0, 0, 0, .4);
}

.meta a {
    text-decoration: none;
    color: rgba(0, 0, 0, .4);
}

.meta a:hover {
    color: rgba(0, 0, 0, .87);
}
</style>



<script src="js/jquery-2.1.1.js"></script>
<script src="js/velocity.min.js"></script>
<script src="js/main.js"></script>


@endsection
