@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12 " style="margin-top:0px">

          @if (session('mensaje'))
          <div class="alert alert-success" id="success-alert" style="margin-top:0px">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              {{session('mensaje')}}
          </div>

          <script type="text/javascript">
          $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
              $("#success-alert").slideUp(500);
          });(2000, 500).slideUp(500, function(){
              $("#success-alert").slideUp(500);
          });
            </script>
          @endif


            <div class="panel panel-default">
                <div class="panel-heading" style="min-height:50px">
                  <label style="font-size:25px">Mi lista de equipos</label>
                  <div class="pull-right">
                    <a href="" data-toggle="modal" data-target="#modallista" data-remote="false" class="btn btn-default ">Lista en texto <i class="fa fa-list"></i></a>
                    <a href="" data-toggle="modal" data-target="#modalnuevo" data-remote="false" class="btn btn-default ">Agregar nuevo <i class="fa fa-laptop"></i></a>

                  </div>

                </div>
                <div class="panel-body" style="min-height:500px">




                  <table class="table">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Promoción</th>

                      </tr>
                    </thead>
                    <tbody>

                      @foreach ($equipos as $e)

                      <tr>
                        <td>{{$e->titulo}} </td>
                        <td>${{$e->precio}}</td>
                        <td>
                          <div class="btn-group btn-group-justified" style="max-width:115px">
                            @if ($e->promo == null)
                              <a href="/admin/equipos/setpromo/{{$e->id}}" class="btn btn-default ">Desactivada</a>
                            @else
                              <a href="/admin/equipos/setpromo/{{$e->id}}" class="btn btn-success ">Activada</a>
                            @endif
                          </div>
                        </td>

                        <td>
                          <div class="pull-right">
                            <a href="/equipos/{{$e->id}}"  data-remote="false" class="btn btn-success"> Ver </a>
                            <a href="" data-toggle="modal" data-target="#modaleditar{{$e->id}}" data-remote="false" class="btn btn-primary"> Editar <i class="fa fa-pencil"></i></a>
                            <a href="" data-toggle="modal" data-target="#modaldelete{{$e->id}}" data-remote="false" class="btn btn-danger "> <i class="fa fa-trash"></i></a>

                          {{-- <form id='yt-player' action='/admin/equipos/delete' method='post'>
                            <input type="hidden" name="id" value="{{$e->id}}">
                            <a href="" data-toggle="modal" data-target="#modaleditar{{$e->id}}" data-remote="false" class="btn btn-primary">Editar <i class="fa fa-pencil"></i></a>
                            <input type='submit'  class='btn btn-danger' value='Eliminar' style="color:white" />
                                {!! csrf_field() !!}
                            </form> --}}
                          </div>

                        </td>
                      </tr>

                      <div class="modal fade" id="modaldelete{{$e->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 100px;" >
                                <div class="modal-dialog" style="  ">
                                  <div class="modal-content" style=" background-color:#E44332">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color:white" aria-hidden="true">&times;</span></button>
                                      <h5 class="modal-title" id="myModalLabel" style="color:white"><strong>Eliminar equipo "{{$e->titulo}}"</strong></h5>
                                    </div>
                                    <div class="modal-body">
                                      <div class="row">
                                        <div class=" col-md-12">

                                                <h5 style="color:white">Estás a punto borrar el equipo, la acción no se puede deshacer</h5><br>
                                            <div style="text-align: right">
                                              <a href="/admin/equipos/delete/{{$e->id}}" class="btn btn-link" style="color:white">Eliminar </a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

                                              </form>
                                            </div>


                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>




                            <div class="modal fade" id="modaleditar{{$e->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
                              <div class="modal-dialog" style="min-width:500px">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title" id="myModalLabel">Editar equipo</h3>
                                  </div>

                                  <div class="modal-body">
                                    <form  action='/admin/equipos/update' method='post'>

                                    <div class="row">

                                      <div class=" col-md-12">


                                      <div class="row"></div>


                                              {{-- <h4>Crear nuevo </h4><br> --}}

                                              <input type='text'  class='form-control ' value="{{$e->titulo}}" required name='titulo' placeholder=""> <br>

                                              <label for="desc">Descripcion del equipo</label>
                                              <textarea name="desc" class="form-control" rows="14" required cols="50">{{$e->desc}}</textarea><br>
                                              <input type="hidden" name="id" value="{{$e->id}}">
                                              <div class="row">
                                                <div class="col-sm-12">
                                                  <label for="titulo">Precio</label>
                                                  <input type='number' min="0" value="{{$e->precio}}" class='form-control input-lg' required name='precio' placeholder="$0000"> <br>

                                                </div>
                                              </div>
                                              {!! csrf_field() !!}

                                      </div>

                                      {{-- <div class="col-md-6 " style="min-height:500px" >


                                        <ul class="list-group">

                                          <li class="list-group-item">
                                            <label for="">Seleccionar imagen principal</label>
                                            <select class="form-control" name="selectimgprin">
                                              <option value="">Imagen 1</option>
                                              <option value="">Imagen 2</option>
                                              <option value="">Imagen 3</option>
                                            </select>
                                          </li>


                                          <li class="list-group-item" style="align:center">
                                            <a target="_blank" href="{{url('/')}}/images/{{$e->img1}}" class="btn btn-default">
                                              Imagen 1
                                            </a>
                                            <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                          </li>

                                        </ul>

                                      </div> --}}



                                      <div class="col-md-12" style="text-align: right">
                                          <button type="button" class="btn btn-link btn-lg" data-dismiss="modal">Cancelar</button>
                                          <button type="submit" class="btn btn-lg btn-primary" value="Submit">Guardar <i class="fa fa-save"></i></button>

                                      </div>


                                    </div>
                                  </form>


                                  </div>

                                </div>
                              </div>
                            </div>

                            <script type="text/javascript">
                            function readURL1AT{{$e->id}}(input) {
                              alert('123123');
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();

                                    reader.onload = function (e) {
                                        $('#img1-{{$e->id}}')
                                            .show()
                                            .attr('src', e.target.result)

                                        $('#selectfile1AT{{$e->id}}').css('background-color', '#58c86a');
                                        $('#selectfile1AT{{$e->id}}').css('color', '#fff');
                                        $('#selectfile1AT{{$e->id}}').prop( 'value','Imagen seleccionada ☑️');

                                    };

                                    reader.readAsDataURL(input.files[0]);
                                }
                            }

                            </script>







                    @endforeach



                    </tbody>
                  </table>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalnuevo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
  <div class="modal-dialog" style="min-width:700px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Agregar equipo</h3>
      </div>

      <div class="modal-body">
        <form  action='/admin/equipos' enctype="multipart/form-data" method='post'>

        <div class="row">

          <div class=" col-md-6">


          <div class="row"></div>


                  {{-- <h4>Crear nuevo </h4><br> --}}

                  <label for="titulo">Título del equipo</label>
                  <input type='text'  class='form-control ' required name='titulo' placeholder=""> <br>

                  <label for="desc">Descripcion del equipo</label>
                  <textarea name="desc" class="form-control" rows="15" required cols="50"></textarea><br>

                  <div class="row">
                    <div class="col-sm-6">
                      <label for="titulo">Precio</label>
                      <input type='number' min="0"  class='form-control input-lg' required name='precio' placeholder="$0000"> <br>

                    </div>
                  </div>

          </div>

          <script type="text/javascript">
          $(document).ready(function(){

           $('#fileu').change(function(){
              alert("Prro");
           })

          });
          </script>

          <div class="col-md-6 " style="min-height:500px" >


            <label for="titulo">Imágenes</label>

              <ul class="list-group">
                <li class="list-group-item" style="align:center">
                  {{-- <img class="img-thumbnail" id="img1" src="#" style="display:none; margin-bottom:10px;  width: auto; height: 120px;" /> --}}
                  {{-- <span class="btn btn-default btn-file"  > --}}

                  <input class="btn btn-default"  type="file"  multiple name="images[]" required accept="image/*">

                  {{-- </span> --}}

                  <div id="file-list">
                  </div>

                </li>

              </ul>


              {!! csrf_field() !!}


          </div>



          <div class="col-md-12" style="text-align: right">
              <button type="button" class="btn btn-link btn-lg" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-lg btn-primary" value="Submit">Guardar <i class="fa fa-save"></i></button>

          </div>


        </div>
      </form>


      </div>

    </div>
  </div>
</div>


<div class="modal fade" id="modallista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 10px">
  <div class="modal-dialog" style="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Lista de equipos</h3>

      </div>

      <div class="modal-body">
        <div class="row">
          <div class=" col-md-12">

<textarea name="desc"  class="form-control" id="lista" rows="20" required cols="50">

******************************************************
@foreach($equipos as $e)
{{$e->titulo}}

{{$e->desc}}

${{$e->precio}}
******************************************************

@endforeach

************************************************************************************
************************************************************************************
************************************************************************************
Notas:

Los equipos en esta lista son usados, a menos que en la descripcion diga que es nuevo.
Y todo cuenta solo con 3 dias habiles de garantia apartir del momento en que sea recibido.

El envio tiene costo, y lo cubre el comprador.

En el caso de envios es responsabilidad del comprador solicitar seguro si lo desea,
el seguro es por si el paquete se pierde o es robado despues de que lo entrego a paqueteria.

*************************************************************************************
*************************************************************************************
*************************************************************************************
                  </textarea><br>

                  <script type="text/javascript">
                  function copialista() {
                    $("#lista").select();
                    document.execCommand('copy');
                    $("#copia").html('TEXTO COPIADO <i class="fa  fa-thumbs-o-up"></i>');
                    $("").select();
                  }


                  </script>
                  <div class="pull-right">
                    <button type="button" id="copia" onClick="copialista()" class="btn btn-default btn-lg" name="button">Copiar lista <i class="fa fa-copy"></i></button>
                  </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>






@endsection
