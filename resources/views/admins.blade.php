@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12 " style="margin-top:0px">

          @if (session('mensaje'))
          <div class="alert alert-success" id="success-alert" style="margin-top:0px">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              {{session('mensaje')}}
          </div>

          <script type="text/javascript">
          $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
              $("#success-alert").slideUp(500);
          });
          $("#danger-alert").fadeTo(2000, 500).slideUp(500, function(){
              $("#success-alert").slideUp(500);
          });
            </script>
          @endif


            <div class="panel panel-default">
                <div class="panel-heading" style="min-height:67px">
                  <label style="font-size:25px">Administradores</label>

                  <div class="pull-right">
                    <a href="" data-toggle="modal" data-target="#modalnuevo" data-remote="false" class="btn btn-lg btn-default ">Agregar administrador <i class="fa fa-user"></i></a>

                  </div>

                </div>
                <div class="panel-body" style="min-height:500px">




                  <table class="table">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Email</th>

                      </tr>
                    </thead>
                    <tbody>

                      @foreach ($admins as $e)

                      <tr>
                        <td>{{$e->name}}</td>
                        <td>{{$e->email}} </td>

                        <td>
                          <div class="pull-right">
                            <a href="" data-toggle="modal" data-target="#modaleditar{{$e->id}}" data-remote="false" class="btn btn-primary "> Editar <i class="fa fa-pencil"></i></a>
                            <a href="" data-toggle="modal" data-target="#modaldelete{{$e->id}}" data-remote="false" class="btn btn-danger "> <i class="fa fa-trash"></i></a>

                          {{-- <form id='yt-player' action='/admin/equipos/delete' method='post'>
                            <input type="hidden" name="id" value="{{$e->id}}">
                            <a href="" data-toggle="modal" data-target="#modaleditar{{$e->id}}" data-remote="false" class="btn btn-primary">Editar <i class="fa fa-pencil"></i></a>
                            <input type='submit'  class='btn btn-danger' value='Eliminar' style="color:white" />
                                {!! csrf_field() !!}
                            </form> --}}
                          </div>

                        </td>
                      </tr>

                      <div class="modal fade" id="modaldelete{{$e->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 100px;" >
                                <div class="modal-dialog" style=" ">
                                  <div class="modal-content" style=" background-color:#E44332">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color:white" aria-hidden="true">&times;</span></button>
                                      <h5 class="modal-title" id="myModalLabel" style="color:white"><strong>Eliminar administrador "{{$e->name}}"</strong></h5>
                                    </div>
                                    <div class="modal-body">
                                      <div class="row">
                                        <div class=" col-md-12">

                                                <h5 style="color:white">Estás a punto de eliminar un administrador, la acción no se puede deshacer</h5><br>
                                            <div style="text-align: right">

                                              <form  action="/admin/users/delete" method="post">
                                                <input type="hidden"  style="display: none"name="id" value="{{$e->id}}">
                                                <button class="btn btn-link" style="color:white" type="submit" name="button">Eliminar <i class="fa fa-trash"></i></button>
                                                {!! csrf_field() !!}

                                              </form>

                                              {{-- <a href="/admin/users/delete/{{$e->id}}" class="btn btn-link" style="color:white">Eliminar </a> --}}
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

                                              </form>
                                            </div>


                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>



                            <div class="modal fade" id="modaleditar{{$e->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
                              <div class="modal-dialog" style="min-width:500px; position: fixed;top: 45%;left: 50%;transform: translate(-50%, -50%);">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title" id="myModalLabel">Editar administrador</h3>
                                  </div>

                                  <div class="modal-body">
                                    <form  action='/admin/users/update' method='post'>

                                    <div class="row">

                                      <div class=" col-md-12">


                                      <div class="row"></div>


                                              {{-- <h4>Crear nuevo </h4><br> --}}
                                              <input type='hidden'  class='form-control ' value="{{$e->id}}" required name='id' placeholder=""> <br>

                                              <label for="titulo">Nombre</label>
                                              <input type='text'  class='form-control ' value="{{$e->name}}" required name='name' placeholder=""> <br>
                                              <label for="titulo">E-mail</label>
                                              <input type='email'  class='form-control ' value="{{$e->email}}" required name='email' placeholder=""> <br>


                                              <div class="row">
                                                <div class="col-sm-12">
                                                  <label for="titulo">Contraseña (Si no deseas cambiar la contraseña deja éste campo vacío)</label>
                                                  <input type='text' min="0"  class='form-control input-lg'  name='passwordupdateuser{{$e->id}}' placeholder="******"> <br>


                                                </div>

                                              </div>
                                              {!! csrf_field() !!}

                                      </div>





                                      <div class="col-md-12" style="text-align: right">
                                          <button type="button" class="btn btn-link btn-lg" data-dismiss="modal">Cancelar</button>
                                          <button type="submit" class="btn btn-lg btn-primary" value="Submit">Guardar <i class="fa fa-save"></i></button>

                                      </div>


                                    </div>
                                  </form>


                                  </div>

                                </div>
                              </div>
                            </div>








                    @endforeach



                    </tbody>
                  </table>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalnuevo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
  <div class="modal-dialog" style="min-width:500px; position: fixed;top: 45%;left: 50%;transform: translate(-50%, -50%);">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Agregar administrador</h3>
      </div>

      <div class="modal-body">
        <form  action='/admin/users' method='post'>

        <div class="row">

          <div class=" col-md-12">


          <div class="row"></div>


                  {{-- <h4>Crear nuevo </h4><br> --}}

                  <label for="titulo">Nombre</label>
                  <input type='text'  class='form-control ' required name='name' placeholder=""> <br>
                  <label for="titulo">E-mail</label>
                  <input type='email'  class='form-control ' required name='email' placeholder=""> <br>


                  <div class="row">
                    <div class="col-sm-6">
                      <label for="titulo">Contraseña</label>
                      <input type='text' min="0"  class='form-control input-lg' required name='password' placeholder="******"> <br>


                    </div>

                  </div>
                  {!! csrf_field() !!}

          </div>





          <div class="col-md-12" style="text-align: right">
              <button type="button" class="btn btn-link btn-lg" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-lg btn-primary" value="Submit">Guardar <i class="fa fa-save"></i></button>

          </div>


        </div>
      </form>


      </div>

    </div>
  </div>
</div>


<div class="modal fade" id="modallista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 10px">
  <div class="modal-dialog" style="min-width:700px;position:fixed; top: 45%;left: 50%;transform: translate(-50%, -50%);">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Lista de equipos</h3>

      </div>



    </div>
  </div>
</div>




@endsection
