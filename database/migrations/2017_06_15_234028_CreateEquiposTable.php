<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('desc');
            $table->string('condicion');
            $table->string('precio' );
            $table->string('procesador');
            $table->string('ram' );
            $table->string('gpu' );
            $table->string('hdd' );
            $table->string('envio' );
            $table->string('tamPantalla');
            $table->string('teclado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
}
