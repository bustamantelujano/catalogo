<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model{
  protected $fillable = [
        'id',
        'titulo',
        'desc',
        'img1' ,
        'img2' ,
        'img3' ,
        'precio' 
  ];

  protected $hidden = [
      'created_at', 'updated_at',
  ];
}
