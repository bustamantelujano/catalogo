<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use App\ImageEquipo;
use App\Equipo;
class HomeController extends Controller{
  public function index(){
    $equipos = DB::table('equipos')->orderBy('promo', 'DESC')->get();
    return view('welcome')->with('equipos', $equipos);
  }
  public function getFirstPhoto($id){

  }
  public function rh(){
    return redirect('/admin');
  }
  public function register(){
    return redirect('/login');
  }
  public function getequipo($id){
    $equipo = Equipo::find($id);
     if (!$equipo)  return redirect('/')->with('mensaje', "Equipo no encontrado");
    $images = DB::table('image_equipos')->where('id_equipo', '=', $id)->get();
    $promo = DB::table('equipos')->where('promo', '=', '1')->limit(4)->get();
    // dd($promo);

    return view('detalleequipo')->with('equipo', $equipo)->with('images', $images)->with('promo', $promo);
  }
}
