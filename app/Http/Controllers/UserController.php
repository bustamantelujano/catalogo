<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\Equipo;
use App\User;
use App\ImageEquipo;
use Auth;

class UserController extends Controller{
  public function __construct(){
      $this->middleware('auth');
  }
  public function index(){
      return view('home');
  }
  public function getadmindashboard(){
      $equipos = Equipo::all();

      return view('home')->with('equipos', $equipos);
  }
  public function nuevoequipo(Request $request){
    if (!is_dir(base_path() . '/public/images/')) {
        mkdir(base_path() . '/public/images/');
    }
    $destinationPath = base_path() . '/public/images/';
    $newequipo = Equipo::create(Input::all());
    if ( Input::hasFile('images') ) {
        $files = Input::file('images');
        foreach($files as $file){
          $filename = $this->GUID() . "." . strtolower($file->getClientOriginalExtension());
          $file->move($destinationPath, $filename);
          $imagen = new ImageEquipo;
          $imagen->id_equipo = $newequipo->id;
          $imagen->name = $file->getClientOriginalName();
          $imagen->filename = $filename;
          $imagen->save();
        }
      }
      $imgPrin = DB::table('image_equipos')
                     ->where('id_equipo', '=', $newequipo->id)
                     ->orderBy('name')
                     ->first();
      $newequipo->img_principal = $imgPrin->filename;
      $newequipo->save();
    return back()->with('mensaje', 'Equipo creado correctamente');
  }
  public function updateequipo(Request $request){
    $equipo = Equipo::find(Input::get('id'));
    $equipo->titulo = Input::get('titulo');
    $equipo->desc = Input::get('desc');
    $equipo->precio = Input::get('precio');
    $equipo->save();
    return back()->with('mensaje', 'Equipo editado correctamente');
  }
  public function setpromo(Request $request,$id){
    $equipo = Equipo::find($id);
    $equipo->promo = $equipo->promo == null ? 1 : null;
    $equipo->update();
    return back()->with('mensaje', 'Promoción '. ($equipo->promo == 1 ? 'activada' : 'desactivada') .' a equipo ' . $equipo->titulo );
  }
  public function deleteequipo(Request $request,$id){
    $equipo = Equipo::find($id);
    $images = ImageEquipo::where('id_equipo', $id)->get();
    foreach ($images as $i) {
      if (file_exists(base_path() .'/public/images/'. $i->filename) ){
        unlink(base_path() .'/public/images/'. $i->filename);
      }
    }
    ImageEquipo::where('id_equipo', $id)->delete();
    Equipo::destroy($id);
    return redirect('/admin')->with('mensaje','Equipo eliminado correctamente.');
  }
  public function adminusers(){
     $admins =  DB::table('users')
            ->where('id', '!=', '1')
            ->where('id', '!=', Auth::user()->id)
            ->get();
    //$admins = User::all();
    return view('admins')->with('admins', $admins);
  }
  public function deleteadminuser(Request $request){
    User::destroy(Input::get('id'));
    return back()->with('mensaje','Administrador eliminado correctamente.');
  }
  public function createadminuser(Request $request){
    $user = new User;
      $user->name = $request->input('name');
      $user->email = $request->input('email');
      $user->password = bcrypt($request->input('password'));
      $user->passwordenc = $request->input('password');
      $user->save();
    $admins = User::all();
    $admins = User::where('id','!=', Auth::user()->id)->get();
    return back()->with('mensaje','Administrador creado correctamente.');
  }
  public function updateadminuser(Request $request){
    $user = User::find(Input::get('id'));
    $user->name = Input::get('name');
    $user->email = Input::get('email');
    if (strlen($request->input('passwordupdateuser'.$user->id)) > 5 ) {
      $user->password = bcrypt($request->input('passwordupdateuser'.$user->id));
    }
    $user->update();
    return back()->with('mensaje', 'El cliente fue editado correctamente');
  }
  public function getimage($id){
    return response()->file('/'.'images/'.$id);
  }
  function GUID(){
    if (function_exists('com_create_guid') === true) return trim(com_create_guid(), '{}');
    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
  }
  public function prueba(){
    return view('prueba');
  }
}
