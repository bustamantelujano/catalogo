<?php

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@rh');
Route::get('/register', 'HomeController@register');
Route::get('/equipos/{id}', 'HomeController@getequipo');

Route::get('/admin', 'UserController@getadmindashboard');
Route::get('/admin/equipos/setpromo/{id}', 'UserController@setpromo');

Route::get('/admin/users', 'UserController@adminusers');
Route::post('/admin/users', 'UserController@createadminuser');
Route::post('/admin/users/update', 'UserController@updateadminuser');
Route::post('/admin/users/delete', 'UserController@deleteadminuser');
Route::post('/admin/equipos', 'UserController@nuevoequipo');
Route::post('/admin/equipos/update', 'UserController@updateequipo');
//Route::get('/admin/equipos/update/{id}', 'UserController@updateequipo');
Route::get('/admin/equipos/delete/{id}', 'UserController@deleteequipo');
Route::post('/admin/equipos/imagenes', 'UserController@uploadimagenes');
// Route::get('/images/{id}', 'UserController@getimage');
Route::get('/prueba', 'UserController@prueba');
